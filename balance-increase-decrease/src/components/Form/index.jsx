import { Component } from "react";
import Button from "../Button";

export default class Form extends Component {

    state = {
        value: ''
    }

    onSubmit = (e) => { 
        e.preventDefault();
        this.props.onChange(this.state.value)
    }

    onChange = (e) => {
        const { value } = e.target;

        this.setState({
            value
        })
    }

    render = () => (
        <form onSubmit={this.onSubmit}>
            <input
                name="balance"
                type="number"
                placeholder="sum"
                value={this.state.value}
                onChange={this.onChange}
            />
            <Button text="Save" />
        </form>
    )
}