import { Component } from "react";
import Balance from "../Balance";
import Transactions from "../Transactions";
import Form from "../Form";

let id=0;

export default class App extends Component {

  state = {
    balance: 0,
    transactions: []
  }

  // onIncrease = () => {
  //   this.setState((state) => ({
  //     balance: state.balance + 1,
  //     transactions: [{
  //       label: "increase",
  //       value: 1,
  //       id: ++id
  //     }, ...state.transactions]
  //   }))
  // }

  // onDecrease = () => {
  //   this.setState((state) => ({
  //     balance: state.balance + 1,
  //     transactions: [{
  //       label: "decrease",
  //       value: -1,
  //       id: ++id
  //     }, ...state.transactions]
  //   }))
  // }

  onChange = (value) => {
    this.setState((state) => ({
      balance: state.balance + Number(value),
      transactions: [{ value, label: 'change', id: ++id }, ...state.transactions]
    }))
  }

  render = () => (
    <div className="App">
      <Balance balance={this.state.balance} />
      <Form onChange={this.onChange} />
      <hr />
      <Transactions transactions={this.state.transactions} />
    </div>
  )
}