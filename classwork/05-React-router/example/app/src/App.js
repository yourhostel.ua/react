import './App.css';
import {Routes, Route} from "react-router-dom";
import Template from "./components/Template";
import Home from "./components/pages/Home";
import Blog from "./components/pages/Blog";
import Contact from "./components/pages/Contact";
import PageNotFound from "./components/pages/PageNotFound";
import './App.css';

const App = () => {
    return (
        <div className='App'>
            <Routes>
                <Route path='/' element={<Template/>}>
                  <Route index element={<Home/>}/>
                  <Route path='/Blog/*' element={<Blog/>}/>
                  <Route path='/Contact' element={<Contact/>}/>
                  <Route path='/*' element={<PageNotFound/>}/>
                </Route>
            </Routes>
        </div>
    )
}


export default App
