import React, { useState } from 'react';

export default function Home () {
    const [count, setCount] = useState(0);

    console.log(count)
    return (
        <div>
            <h1>Home</h1>
            <p>Ви натиснули {count} разів</p>
            <button onClick={() => setCount(count + 1)}>
                Натисни мене
            </button>
        </div>
    );
}