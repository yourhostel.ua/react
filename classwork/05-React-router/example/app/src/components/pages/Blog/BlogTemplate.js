import {NavLink, Outlet} from 'react-router-dom'

export default function BlogTemplate({arr}) {
    return (
        <>
            <h1>Blog</h1>
            {arr.map((e, i) => <NavLink key={e.id} to={e.id}>post {i+1}</NavLink>)}
            <p>Lorem Blog ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium ad amet at consectetur
                fugiat hic illum in, modi nemo neque numquam, obcaecati odio, sit tempore ut vel veritatis voluptatibus.
            </p>
            <Outlet/>
        </>
    )
}

//className={({isActive})=> isActive ? 'active-Link' : ''}