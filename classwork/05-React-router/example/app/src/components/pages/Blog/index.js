import {Route, Routes} from 'react-router-dom'
import Post from "./Post";
import PageNotFound from "../PageNotFound";
import BlogTemplate from "./BlogTemplate";

export default function Blog() {
    const arr = [
        {text: "post 1 amet, consecisicing elit",id:'1'},
        {text: "post 2 amonsectetur adipi",id:'2'},
        {text: "post 3 consectetur adipisicing elit",id:'3'},
        {text: "post 4 consectetur adipisicing elit",id:'4'},
    ]

    return (<>
            <Routes>
                <Route path='/' element={<BlogTemplate arr={arr}/>}/>
                  <Route path='/:id' element={<Post posts={arr}/>}/>
                  <Route path='/*' element={<PageNotFound />}/>
                <Route />
            </Routes>
        </>
    )
}