import {useParams} from "react-router-dom";
import {useEffect, useState} from "react"

const Post = ({posts}) => {
    const {id} = useParams()
    const [post, setPost] = useState(null)

    useEffect(() => {
        setPost(posts[id - 1])
    },[posts, id])

    return (
        <div>{
            post && (<>
                <h1>Post {id}</h1>
                <div>{post.text}</div>
            </>)
        }
        </div>
    )
}

export default Post