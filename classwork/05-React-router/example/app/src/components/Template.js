import {Outlet, NavLink} from 'react-router-dom'

export default function Template () {
    return (
        <div>
            <nav>
                <NavLink className={({isActive})=> isActive ? 'active-Link' : ''} to='/'>Home</NavLink>
                <NavLink className={({isActive})=> isActive ? 'active-Link' : ''} to='blog'>Blog</NavLink>
                <NavLink className={({isActive})=> isActive ? 'active-Link' : ''} to='contact'> Contact </NavLink>
            </nav>
            <Outlet />
        </div>
    )
}