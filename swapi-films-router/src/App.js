import './App.css';
import {Routes, Route,} from "react-router-dom";
import Films from "./components/Films";
import Index from "./components/Film/FullFilm";

const App = () => (
    <Routes>
        <Route path="/" element={<Films/>}></Route>
        <Route path="/films" element={<Films/>}></Route>
        <Route path="/films/:id" element={<Index/>}></Route>
    </Routes>
)

export default App
