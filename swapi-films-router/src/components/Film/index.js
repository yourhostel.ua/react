import {useState} from "react";
import Detail from "../Detail";
import {Link} from "react-router-dom";

const Film =({name, episodeId, openingCrawl})=> {

    const [clicked] = useState(false);

    return (
        <li>
                          <span>{name}</span>
            {
                clicked ? <Detail episodeId={episodeId} openingCrawl={openingCrawl}/>
                        : <Link to={`films/${episodeId}`}>Детальнее</Link>
            }
        </li>
    )
}

export default Film