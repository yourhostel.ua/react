import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import Loader from "../../Loader";

const Index =()=> {
    const [isLoaded, setIsLoaded] = useState(false),
          [film, setFilm] = useState(null),
          params = useParams();

    useEffect(() => {

        fetch('https://ajax.test-danit.com/api/swapi/films/' + params.id)
            .then(res => res.json())
            .then(data => {setIsLoaded(true);setFilm(data)})

    }, [params]);

    if (!isLoaded) return <Loader/>;

    return <div>Film #{params.id} <p>{film.openingCrawl}</p></div>;
}

export default Index