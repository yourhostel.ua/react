import Film from '../Film';
import {useEffect, useState} from "react";
import Loader from "../Loader";

const Films =()=> {
    const [isLoading, setIsLoading] = useState(true),
          [films, setFilms] = useState([]);

    useEffect(() => {

        fetch('https://ajax.test-danit.com/api/swapi/films')
            .then(res => res.json())
            .then(films => {setFilms(films); setIsLoading(false)})

    }, [])

    if (isLoading) return <Loader/>;

    return <ol>
        {
            films.map(film => (
           <Film key={film.id} name={film.name} episodeId={film.episodeId} openingCrawl={film.openingCrawl}/>
                )
            )
        }
           </ol>
}

export default Films